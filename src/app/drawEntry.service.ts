import { Injectable } from '@angular/core';
import { Observable, Subject } from 'rxjs';
import { WebsocketService } from './websocket.service';
import { DrawEntry, Point } from './drawClasses';
import { map } from 'rxjs/operators';
const CHAT_URL = 'ws:/85.214.56.221:9898/';
const CHAT_URL1 = 'ws:/localhost:9898/';

@Injectable({
  providedIn: 'root',
})
export class DrawEntryService {
  public messages: Subject<DrawEntry[]>;
  constructor(wsService: WebsocketService) {
    console.log("Created DrawEntryService")
    this.messages = <Subject<DrawEntry[]>>wsService.connect(CHAT_URL).pipe(
      map((response: MessageEvent): DrawEntry[] => {
        console.log("I got Data from drawEntryService")
        console.log(response.data);
        let data = JSON.parse(response.data);
        let drawEntries = [];
        data.forEach((sub) => {
          drawEntries.push(...JSON.parse(sub));
        });
        return drawEntries;
      })
    );
  }
}
