
export class DrawEntry {
  type:string;
  user:string;
  startPos:Point;
  endPos:Point;
  color:string;
  size:number;
  constructor(type:string,user:string,startPos:Point,endPos:Point,color:string,size:number){
    this.type=type;
    this.user=user;
    this.startPos=startPos;
    this.endPos=endPos;
    this.color=color;
    this.size=size;
  }
}
export class Point {
  posX:number;
  posY:number;
  constructor(posX:number,posY:number){
    this.posX=posX;
    this.posY=posY;
  }
}
export class WsData{
type:string;
data:[];

}
