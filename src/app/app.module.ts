import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppComponent } from './app.component';
import { HttpClientModule } from '@angular/common/http';
import { DrawControlsComponent } from './draw-controls/draw-controls.component';
import {WebsocketService} from "./websocket.service";
import {WebsocketService2} from "./websocket2.service";
import {DrawEntryService} from "./drawEntry.service";
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
@NgModule({
  declarations: [
    AppComponent,
    DrawControlsComponent
  ],
  imports: [
    BrowserModule,
    HttpClientModule,
    NgbModule
  ],
  providers: [WebsocketService,WebsocketService2,DrawEntryService],
  bootstrap: [AppComponent]
})
export class AppModule { }
