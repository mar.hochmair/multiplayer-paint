import { Injectable } from '@angular/core';
import { Observable, Subject } from 'rxjs';
import { WebsocketService2 } from './websocket2.service';
import { map } from 'rxjs/operators';
const CHAT_URL = 'ws:/85.214.56.221:9899/';
const CHAT_URL1 = 'ws:/localhost:9899/';

@Injectable({
  providedIn: 'root',
})
export class ImageService {
  public messages: Subject<string>;
  constructor(wsService: WebsocketService2) {
    console.log("Created Image Service")
    this.messages = <Subject<string>>wsService.connect(CHAT_URL).pipe(
      map((response: MessageEvent): string => {
        console.log("I got data from image Service")
        console.log(response.data);
        let data = JSON.parse(response.data);
          return data;
      })
    );
  }
}
