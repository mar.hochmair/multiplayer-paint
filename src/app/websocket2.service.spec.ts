import { TestBed } from '@angular/core/testing';

import { WebsocketService2 } from './websocket2.service';

describe('WebsocketService', () => {
  let service: WebsocketService2;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(WebsocketService2);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
