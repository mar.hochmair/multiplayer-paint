import { TestBed } from '@angular/core/testing';

import { DrawEntryService } from './drawEntry.service';

describe('DrawEntryService', () => {
  let service: DrawEntryService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(DrawEntryService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
