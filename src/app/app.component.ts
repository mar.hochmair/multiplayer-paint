import { Component, ElementRef, ViewChild } from '@angular/core';
import { Point, DrawEntry } from './drawClasses';
import { DrawEntryService } from './drawEntry.service';
import { ImageService } from './image-service.service';
declare var $: any;
@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css'],
})
export class AppComponent {
  title = 'multiplayer-paint';
  canvasContext = null;
  mouseDownPos = null;
  drawMethod = 'free';
  @ViewChild('canvasElement') canvasElement: ElementRef;
  mouseDown = true;
  offsetLeft: number;
  drawEntries: DrawEntry[] = [];
  color = '#000000';
  size = 3;
  panning = false;
  refreshRate = 16;
  wsData: DrawEntry[] = [];
  user: string = 'default' + Math.random();
  ngOnInit() {
    console.log('Test');
    this.initCanvas();
  }
  constructor(
    private drawEntryService: DrawEntryService,
    private imageService: ImageService
  ) {
    console.log('Constr!');
    var that = this;
    drawEntryService.messages.subscribe((drawEntries) => {
      if (!drawEntries.length) return;
      console.log('Response From Websocket Server:', drawEntries);
      drawEntries.forEach((drawEntry) => this.drawLine(drawEntry, false, true));
    });

    imageService.messages.subscribe((savedImg) => {
      if (!savedImg.length) return;
      var img = new Image();
      img.onload = function () {
        that.canvasContext.drawImage(img, 0, 0, 800, 800); // Or at whatever offset you like
      };
      img.src = savedImg;
      console.log('Response From Websocket Server:', savedImg);
    });
  }
  setTool(type: string) {
    if (type == 'eraser') {
      this.color = '#FFFFFF';
    } else {
      this.color = '#000000';
    }
  }
  changeSize(direction: number) {
    this.size += direction;
    this.canvasContext.lineWidth = this.size;
  }
  changeColor(color: string) {
    this.color = color;
    this.canvasContext.strokeStyle = this.color;
  }
  togglePanning(panning: boolean) {
    this.panning = panning;
    $('#scrollContainer').css('overflow', panning ? 'scroll' : 'hidden');
  }
  deleteAll() {
    this.drawEntries = [];

    this.canvasContext.clearRect(0, 0, 5000, 5000);
  }
  getRelativePointTouch(e) {
    let scrollTop = $('#scrollContainer').scrollTop();
    let scrollLeft = $('#scrollContainer').scrollLeft();
    let offset = $('#scrollContainer').offset();
    console.log(scrollTop, scrollLeft);
    return new Point(
      e.touches[0].pageX + scrollLeft - offset.left,
      e.touches[0].pageY + scrollTop - offset.top
    );
  }
  drawLine(
    lineDrawEntry: DrawEntry,
    saveToServer: boolean,
    fromWebSocket: boolean
  ) {
    this.canvasContext.lineWidth = lineDrawEntry.size;
    this.canvasContext.strokeStyle = lineDrawEntry.color;
    //console.log('drawLine:', lineDrawEntry);
    if (!fromWebSocket) {
      this.wsData.push(lineDrawEntry);
    }
    this.canvasContext.beginPath();
    this.canvasContext.lineJoin = 'round';
    this.canvasContext.lineCap = 'round';
    this.canvasContext.moveTo(
      lineDrawEntry.startPos.posX,
      lineDrawEntry.startPos.posY
    );
    this.canvasContext.lineTo(
      lineDrawEntry.endPos.posX,
      lineDrawEntry.endPos.posY
    );
    this.canvasContext.stroke();
    if (saveToServer) {
      this.entries++;
      //   this.drawEntriesService.addDrawEntry(lineDrawEntry).subscribe();
    }
  }
  entries = 0;
  drawStart(e) {
    console.log('asd');
    if (e.button == 0 || e.touches.length == 1) {
      this.mouseDown = true;
      this.mouseDownPos =
        e.touches && e.touches.length
          ? this.getRelativePointTouch(e)
          : new Point(e.offsetX, e.offsetY);
    }
  }
  drawEnd(e) {
    //Left Mouse
    if (e.button == 0 || e.button === undefined) {
      this.mouseDown = false;

      if (this.drawMethod == 'line') {
        let lineDrawEntry = new DrawEntry(
          'line',
          this.user,
          this.mouseDownPos,
          new Point(e.offsetX, e.offsetY),
          this.color,
          this.size
        );
        this.drawEntries.push(lineDrawEntry);
        this.drawLine(lineDrawEntry, true, false);
      }
    }
  }
  drawMove(e) {
    if (e.button == 0 || e.touches.length == 1) {
      if (this.drawMethod == 'free' && this.mouseDown) {
        if (this.mouseDownPos) {
          let pos =
            e.touches && e.touches.length
              ? this.getRelativePointTouch(e)
              : new Point(e.offsetX, e.offsetY);

          let lineDrawEntry = new DrawEntry(
            'line',
            this.user,
            this.mouseDownPos,
            pos,
            this.color,
            this.size
          );
          this.mouseDownPos = pos;
          this.drawEntries.push(lineDrawEntry);
          this.drawLine(lineDrawEntry, true, false);
        } 
      }
    }
  }
  initCanvas() {
    setInterval(() => {
      if (this.wsData.length) {
        this.drawEntryService.messages.next(this.wsData);
        this.wsData = [];
      }
    }, this.refreshRate);
    $('#canvas').on('mousedown', (e) => {
      this.drawStart(e);
    });
    $('#canvas').on('mouseup', (e) => {
      this.drawEnd(e);
    });
    $('#canvas').on('mousemove', (e) => {
      this.drawMove(e);
    });
    $('#canvas').bind('touchstart', (e) => {
      if (!this.panning) this.drawStart(e);
    });
    $('#canvas').bind('touchend', (e) => {
      if (!this.panning) this.drawEnd(e);
    });
    $('#canvas').bind('touchmove', (e) => {
      if (!this.panning) this.drawMove(e);
    });
    console.log($('#canvasContainer').clientWidth);
    // $('canvas').attr('width','800px');
    // $('canvas').attr('height','800px');
    this.canvasContext = (document.getElementById('canvas') as any).getContext(
      '2d'
    );

    $('canvas').on('contextmenu', function (e) {
      return false;
    });
    var that = this;

    var rtime = new Date();
    var timeout = false;
    var delta = 200;

    $(window).on('keypress', (e) => {
      if (e.key == 'p') this.deleteAll();
      if (e.key == 'd')
        $('#scrollContainer').scrollLeft(
          $('#scrollContainer').scrollLeft() + 10
        );
      if (e.key == 'a')
        $('#scrollContainer').scrollLeft(
          $('#scrollContainer').scrollLeft() - 10
        );
      if (e.key == 'w')
        $('#scrollContainer').scrollTop($('#scrollContainer').scrollTop() - 10);
      if (e.key == 's')
        $('#scrollContainer').scrollTop($('#scrollContainer').scrollTop() + 10);
    });
    $(window).resize(function () {
      rtime = new Date();
      if (timeout === false) {
        timeout = true;
        setTimeout(resizeend, delta);
      }
    });

    function resizeend() {
      if (+new Date() - +rtime < delta) {
        setTimeout(resizeend, delta);
      } else {
        timeout = false;
        //  $('canvas').attr('width', $('#canvasContainer').width() + 'px');
        // $('canvas').attr('height', $('#canvasContainer').height() + 'px');
        that.drawEntries.forEach((drawEntry) => {
          if (drawEntry.type == 'line') {
            that.drawLine(drawEntry, false, true);
          }
        });
      }
    }
  }
}
