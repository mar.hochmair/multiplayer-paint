import { ComponentFixture, TestBed } from '@angular/core/testing';

import { DrawControlsComponent } from './draw-controls.component';

describe('DrawControlsComponent', () => {
  let component: DrawControlsComponent;
  let fixture: ComponentFixture<DrawControlsComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ DrawControlsComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(DrawControlsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
