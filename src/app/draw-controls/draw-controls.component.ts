import { Component, OnInit, ɵɵresolveBody } from '@angular/core';
import { $ } from 'protractor';
import { AppComponent } from '../app.component';
@Component({
  selector: 'app-draw-controls',
  templateUrl: './draw-controls.component.html',
  styleUrls: ['./draw-controls.component.css'],
})
export class DrawControlsComponent implements OnInit {
  constructor(
    private parent: AppComponent
  ) {}
  panning=false;
  ngOnInit() {
    console.log('Init drawControls!');
  }
  setTool(type: string) {
    this.parent.setTool(type);
  }
  changeSize(direction: number) {
    this.parent.changeSize(direction);
  }
  changeColor(color: string) {
    this.parent.changeColor(color);
  }
  togglePanning(){
    this.panning=!this.panning;
    this.parent.togglePanning(this.panning);
    window.blur();
  }
}
